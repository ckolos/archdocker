all: container commit push

container:
	sudo bash ./arch-container.sh

commit:
	docker commit -a "Chris Kolosiwsky" -m "ArchLinux build for $(date +%F)" `docker ps -a | grep ckolos/arch_base:latest | grep Success | head -n1 | cut -d" " -f1` ckolos/arch_base:latest

push:
	docker push ckolos/arch_base:latest
