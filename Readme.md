Directory contains:
* Dockerfile
  - A Dockerfile intended for automated builds of ckolos/archlinux:latest
* Readme.md
  - This file
* arch-container.sh
  - The arch-container script from the Docker upstream /contrib directory.
    only modified to tag with ckolos/archlinux:latest on build.
* mkimage-arch-pacman.conf
  - pacman config for the arch-container image
